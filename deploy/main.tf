terraform {
  backend "s3" {
    bucket         = "terraform-learning-tfstate"
    key            = "terraform-learning.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-learning-tf-stage-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.9.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
