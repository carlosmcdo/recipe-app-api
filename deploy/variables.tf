variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "carlos.mcdo@gmail.com"
}